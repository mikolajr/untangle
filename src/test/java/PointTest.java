import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class PointTest {

    @Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {1, 1, 1, 1, true},
                {1, 1, 1, 1.1, true},
                {1, 1, 1, 0.9, true},
                {0, 0, 1, 1, false},
        });
    }

    @Parameter(0)
    public double x1;
    @Parameter(1)
    public double y1;
    @Parameter(2)
    public double x2;
    @Parameter(3)
    public double y2;
    @Parameter(4)
    public boolean covers;

    @Test
    public void testCovers() throws Exception {
        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        if (covers) {
            assertTrue(p1.covers(p2, Point.EPSILON));
        } else {
            assertFalse(p1.covers(p2, Point.EPSILON));
        }
    }
}