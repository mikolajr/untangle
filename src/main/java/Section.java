import java.awt.geom.Line2D;

public class Section {

    private Point a, b;

    public Section(Point a, Point b) {
        this.a = a;
        this.b = b;
    }

    public double length() {
        return a.distance(b);
    }

    public Point getA() {
        return a;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Section section = (Section) o;

        if (a != null ? !a.equals(section.a) : section.a != null) return false;
        return !(b != null ? !b.equals(section.b) : section.b != null);

    }

    @Override
    public int hashCode() {
        int result = a != null ? a.hashCode() : 0;
        result = 31 * result + (b != null ? b.hashCode() : 0);
        return result;
    }

    public Point getB() {
        return b;
    }

    private boolean startsEndOnPoint(Point point) {
        return a.equals(point) || b.equals(point);
    }

    public boolean intersects(Section s) {
        Line2D thisLine = new Line2D.Double(a.getX(), a.getY(), b.getX(), b.getY());
        Line2D otherLine = new Line2D.Double(s.a.getX(), s.a.getY(), s.b.getX(), s.b.getY());
        boolean intersects = thisLine.intersectsLine(otherLine);

        if (intersects) {
            intersects = !startsEndOnPoint(s.a) && !startsEndOnPoint(s.b);
        }

        return intersects;
    }
}
