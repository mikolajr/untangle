import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Field {

    private Collection<Vertex> vertexes = Collections.emptyList();
    private int width;
    private int height;
    private int screenPointRadius;

    public void regenerate() {
        vertexes = new SimpleVertexGenerator(screenPointRadius*100.0/width).createVertexes().shuffle().build();
    }

    public Collection<Vertex> getVertexes() {
        return vertexes;
    }

    public Point translateToVirtual(double x, double y) {
        return new Point(x * 100.0 / width, y * 100.0 / height);
    }

    public Point translateToScreen(Point point) {
        return new Point(width/100.0*point.getX(), height/100.0*point.getY());
    }

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Point findCoveredPoint(Point point, int screenPointRadius) {
        double distanceToCheck = screenPointRadius * 100.0 / width;
        for (Vertex vertex : vertexes) {
            Point covered = vertex.covers(point, distanceToCheck);
            if (covered != null) {
                return covered;
            }
        }
        return null;
    }

    public boolean isPlanar() {
        if (vertexes.isEmpty()) {
            return false;
        }

        Set<Section> sections = new HashSet<Section>();
        for (Vertex vertex : vertexes) {
            sections.addAll(vertex.getSections());
        }

        for (Vertex vertex : vertexes) {
            for (Section section : sections) {
                if (vertex.intersects(section))
                    return false;
            }
        }

        return true;
    }

    public boolean isPointAvailable(Point point, int screenPointRadius) {
        double distanceToCheck = screenPointRadius * 100.0 / width;
        for (Vertex vertex : vertexes) {
            if (vertex.covers(point, distanceToCheck) != null)
                return false;
        }
        return true;
    }

    public void setScreenPointRadius(int screenPointRadius) {
        this.screenPointRadius = screenPointRadius;
    }
}
