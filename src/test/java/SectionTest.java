import org.junit.Test;

import static org.junit.Assert.*;

public class SectionTest {

    private static final double DELTA = 0.0001;

    @Test
    public void testLength() throws Exception {
        assertEquals(1.0, new Section(new Point(0,0), new Point(1,0)).length(), DELTA);
        assertEquals(1.0, new Section(new Point(0,1), new Point(1,1)).length(), DELTA);
        assertEquals(Math.sqrt(2), new Section(new Point(0,0), new Point(1,1)).length(), DELTA);
    }

    @Test
    public void testDoesNotIntersects() throws Exception {
        Section s1 = new Section(new Point(0,0), new Point(10,0));

        assertFalse(s1.intersects(new Section(new Point(0, 1), new Point(10, 1))));
        assertFalse(s1.intersects(new Section(new Point(1, 1), new Point(2, 2))));
        assertFalse(s1.intersects(new Section(new Point(-1, -1), new Point(-2, -2))));
        assertFalse(s1.intersects(new Section(new Point(0.1, 0.1), new Point(10, 0.1))));
    }

    @Test
    public void testIntersects() throws Exception {
        Section s1 = new Section(new Point(0,0), new Point(10,0));

        assertTrue(s1.intersects(new Section(new Point(0, 1), new Point(10, -1))));
        assertTrue(s1.intersects(new Section(new Point(5, -1), new Point(3, 3))));
    }

    @Test
    public void testLinesAreParallel() throws Exception {
        Section s1 = new Section(new Point(0,0), new Point(10,0));

        assertFalse(s1.intersects(new Section(new Point(0, 1), new Point(10, 1))));
        assertFalse(s1.intersects(new Section(new Point(11, 0), new Point(12, 0))));
        assertFalse(s1.intersects(new Section(new Point(-5, 0), new Point(-1, 0))));
        assertFalse(s1.intersects(new Section(new Point(0, -2), new Point(3, -2))));

        assertTrue(s1.intersects(new Section(new Point(5, 0), new Point(11, 0))));
        assertTrue(s1.intersects(new Section(new Point(5, 0), new Point(6, 0))));
        assertTrue(s1.intersects(new Section(new Point(-5, 0), new Point(1, 0))));
    }

    @Test
    public void testLinesAreOrthogonal() throws Exception {
        Section s1 = new Section(new Point(0,0), new Point(10,0));

        assertFalse(s1.intersects(new Section(new Point(0, 1), new Point(10, 1))));
    }

    @Test
    public void testCommonPointsOnStartAndEndMakesLineNotIntersect() throws Exception {
        Section s1 = new Section(new Point(0,0), new Point(10,0));

        assertFalse(s1.intersects(new Section(new Point(0, 0), new Point(1, 1))));
        assertTrue(s1.intersects(new Section(new Point(5, 0), new Point(1, 1))));
    }
}