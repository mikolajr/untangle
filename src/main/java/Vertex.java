import java.util.Collection;
import java.util.HashSet;

public class Vertex {

    private Point v;
    private Collection<Section> sections = new HashSet<Section>();

    public Vertex(Point v) {
        this.v = v;
    }

    public Point getPoint() {
        return v;
    }

    public Collection<Section> getSections() {
        return sections;
    }

    public void setSections(Collection<Section> sections) {
        this.sections = sections;
    }

    public boolean add(Point point) {
        return sections.add(new Section(v, point));
    }

    public boolean remove(Section o) {
        return sections.remove(o);
    }

    public int size() {
        return sections.size();
    }

    public Point covers(Point p, double distanceToCheck) {
        if (v.covers(p, distanceToCheck)) {
            return v;
        }
        for (Section section : sections) {
            if (section.getB().covers(p, distanceToCheck)) {
                return section.getB();
            }
        }
        return null;
    }

    public boolean intersects(Section section) {
        for (Section s : sections) {
            if (s.intersects(section))
                return true;
        }
        return false;
    }
}
