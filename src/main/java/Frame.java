import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Frame extends JFrame {

    private UntanglePane untanglePane;

    public Frame() throws HeadlessException {
        super("Untangle");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setBackground(Color.BLACK);
        setMinimumSize(new Dimension(400, 400));

        setLayout(new BorderLayout());

        untanglePane = new UntanglePane();
        getContentPane().add(new BlackPanel(), BorderLayout.NORTH);
        getContentPane().add(new BlackPanel(), BorderLayout.SOUTH);
        getContentPane().add(new BlackPanel(), BorderLayout.WEST);
        getContentPane().add(new BlackPanel(), BorderLayout.EAST);
        getContentPane().add(untanglePane, BorderLayout.CENTER);

        pack();

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        untanglePane.newGame();
                    }
                });
            }
        });
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                untanglePane.newGame();
            }
        });
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Frame().setVisible(true);
            }
        });
    }

    private class BlackPanel extends JPanel {
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }
}
