import java.util.*;

public class SimpleVertexGenerator implements VertexGenerator {

    private final static int NO_OF_POINTS = 10;
    private final static double MIN_DISTANCE = 10.0;
    private static final int MAX_VERTEX_SIZE = 2;

    private Random random = new Random();
    private List<Vertex> vertexes;
    private double screenPointRadius;

    public SimpleVertexGenerator(double screenPointRadius) {
        this.screenPointRadius = screenPointRadius;
    }

    public VertexGenerator createVertexes() {
        List<Point> points = createPoints();

        vertexes = new ArrayList<Vertex>();
        List<Vertex> stack = new ArrayList<Vertex>();

        for (Point point : points) {
            Vertex vertex = new Vertex(point);
            vertexes.add(vertex);

            for (Vertex stackVertex : stack) {
                stackVertex.add(point);
            }
            Iterator<Vertex> iterator = stack.iterator();
            while (iterator.hasNext()) {
                Vertex next = iterator.next();
                if (next.size() == MAX_VERTEX_SIZE) {
                    iterator.remove();
                }
            }

            stack.add(vertex);
        }

        for (int i = vertexes.size() - 1; i >= 0; i--) {
            Vertex vertex = vertexes.get(i);
            for (int j = i - 1; j >= 0 && vertex.size() < MAX_VERTEX_SIZE; j--) {
                if (j > 0) {
                    vertex.add(vertexes.get(j).getPoint());
                }
            }
        }

        return this;
    }

    public VertexGenerator shuffle() {
        Set<Point> existingPoints = new HashSet<Point>();
        Set<Point> newPoints = new HashSet<Point>();
        for (Vertex vertex : vertexes) {
            existingPoints.add(vertex.getPoint());
        }
        while (newPoints.size() < existingPoints.size()) {
            Point point = new Point(random.nextDouble() * 100.0, random.nextDouble() * 100.0);
            boolean isSafeToAdd = true;
            for (Point newPoint : newPoints) {
                if (newPoint.distance(point) <= screenPointRadius) {
                    isSafeToAdd = false;
                    break;
                }
            }
            if (isSafeToAdd) {
                Point existingPoint = existingPoints.iterator().next();
                existingPoints.remove(existingPoint);
                existingPoint.setX(point.getX());

                existingPoint.setY(point.getY());
                newPoints.add(existingPoint);
            }
        }
        return this;
    }

    public Collection<Vertex> build() {
        return vertexes;
    }

    private List<Point> createPoints() {
        List<Point> points = new ArrayList<Point>();
        while (points.size() < NO_OF_POINTS) {
            Point point = new Point(random.nextDouble() * 100.0, random.nextDouble() * 100.0);
            boolean isSafeToAdd = true;
            for (Point existingPoint : points) {
                if (existingPoint.distance(point) <= MIN_DISTANCE) {
                    isSafeToAdd = false;
                    break;
                }
            }
            if (isSafeToAdd) {
                points.add(point);
            }
        }

        Collections.sort(points, new Comparator<Point>() {
            public int compare(Point o1, Point o2) {
                return Double.compare(o1.getX(), o2.getX());
            }
        });
        return points;
    }
}
