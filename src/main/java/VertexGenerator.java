import java.util.Collection;

public interface VertexGenerator {
    VertexGenerator createVertexes();

    VertexGenerator shuffle();

    Collection<Vertex> build();
}
