import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;

public class UntanglePane extends JPanel {

    private static final int POINT_SIZE = 8;

    private Field field;
    private Point dragging;
    private boolean gameInProgress;

    public UntanglePane() {
        field = new Field();
        field.setScreenPointRadius(POINT_SIZE/2);
        setBackground(Color.BLACK);
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                field.setSize(getWidth(), getHeight());
            }

            @Override
            public void componentResized(ComponentEvent e) {
                field.setSize(getWidth(), getHeight());
            }
        });
        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (dragging != null) {
                    int x = e.getX();
                    int y = e.getY();
                    if (x < 0) x=0;
                    if (x>getWidth())x=getWidth();
                    if (y<0) y=0;
                    if (y>getHeight())y=getHeight();
                    doDragging(x, y);
                }
            }
        });
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                endDragging();
            }
            @Override
            public void mousePressed(MouseEvent e) {
                startDragging(e.getX(), e.getY());
            }
        });
    }

    private void endDragging() {
        if (dragging != null) {
//            bouncePoint(dragging);
//            bouncePoint2(dragging);
            bouncePoint3(dragging);
        }
        dragging = null;
        if (gameInProgress && field.isPlanar()) {
            JOptionPane.showMessageDialog(this, "Super");
            gameInProgress = false;
        }
    }

    private void bouncePoint(Point point) {
        final Point copy = point;
        final int[][] moves = new int[][] { {0,5}, {5,-5}, {-5,-5}, {-5,5}, {5,5}, {0,-5} };
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    for (int[] move : moves) {
                        Graphics graphics = getGraphics();
                        Thread.sleep(100);

                        Point virtual = field.translateToVirtual(move[0], move[1]);

                        copy.setX(copy.getX() + virtual.getX());
                        copy.setY(copy.getY() + virtual.getY());
                        paintComponent(graphics);
                        graphics.dispose();
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        });
    }

    private void bouncePoint2(final Point point) {
        final double GRAVITY = 2.0;

        final Point original = new Point(point);
        final Point copy = point;
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                double speed = 0;
                Graphics graphics = getGraphics();
                try {
                    copy.setY(copy.getY()-5);
                    long endTime = System.currentTimeMillis() + 1000;
                    while (System.currentTimeMillis() < endTime) {
                        Thread.sleep(100);

                        speed += GRAVITY;
                        double newY = copy.getY();
                        if (newY > original.getY()+5) {
                            speed = -speed/4.0;
                        }
                        if (newY < original.getY()) {
                            speed = -speed/2.0;
                        }
                        newY += speed;

                        copy.setY(newY);
                        paintComponent(graphics);

                        if (Math.abs(speed) < 0.01)
                            break;
                    }

                    point.setY(original.getY());
                    paintComponent(graphics);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } finally {
                    if (graphics != null) {
                        graphics.dispose();
                    }
                }
            }
        });
    }

    private void bouncePoint3(final Point point) {
        final Point original = new Point(point);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    Graphics graphics = getGraphics();
                    Random random = new Random();
                    for (int i=0; i<30; i++) {
                        Thread.sleep(10);

                        Point virtual = field.translateToVirtual(random.nextDouble()*6-3, random.nextDouble()*6-3);

                        point.setX(original.getX() + virtual.getX());
                        point.setY(original.getY() + virtual.getY());
                        paintComponent(graphics);
                    }
                    point.setX(original.getX());
                    point.setY(original.getY());
                    graphics.dispose();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        });
    }

    private void doDragging(int x, int y) {
        Point point = field.translateToVirtual(x, y);
        if (field.isPointAvailable(point, POINT_SIZE/2)) {
            dragging.setX(point.getX());
            dragging.setY(point.getY());
            repaint();
        }
    }

    private void startDragging(int x, int y) {
        dragging = null;
        Point point = field.translateToVirtual(x, y);
        Point covered = field.findCoveredPoint(point, POINT_SIZE/2);
        if (covered != null) {
            dragging = covered;
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (Vertex vertex : field.getVertexes()) {
            Point screen = field.translateToScreen(vertex.getPoint());
            g.setColor(Color.YELLOW);
            g.drawOval((int)screen.getX() -POINT_SIZE/2, (int)screen.getY() -POINT_SIZE/2, POINT_SIZE, POINT_SIZE);

            for (Section section : vertex.getSections()) {
                Point end = field.translateToScreen(section.getB());
                g.setColor(Color.CYAN);
                g.drawLine((int)screen.getX(), (int)screen.getY(), (int)end.getX(), (int)end.getY());
            }
        }
    }

    public void newGame() {
        gameInProgress = true;
        field.regenerate();
        dragging = null;
        repaint();
    }
}
